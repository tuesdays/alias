package main

import (
        "embed"
	"fmt"
	"log"
	"net/http"
	"os"
	"text/template"
	"encoding/json"

	"flag"

	"gitlab.com/tuesdays/alias"
)

type AliasData struct {
	Alias string `json:"alias"`
}

//go:embed layout.html
//go:embed favicon.ico
var content embed.FS

func main() {
	hostPtr := flag.String("host", "0.0.0.0", "Host address to listen on")
	portPtr := flag.Int("port", 8080, "Port to listen on")
	httpsPtr := flag.Bool("https", false, "Serve over HTTPS")
	pubPtr := flag.String("public", "", "Public key (.pem) - Requires https flag")
	keyPtr := flag.String("private", "", "Private key (.pem) - Requires https flag")
	flag.Parse()

	socket := fmt.Sprintf("%s:%d", *hostPtr, *portPtr)

	mux := http.NewServeMux()
	mux.HandleFunc("/", handler)
	mux.HandleFunc("/api", apiHandler)
	mux.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {
		favicon, _ := content.ReadFile("favicon.ico")
		w.Write(favicon)
	})

	server := http.Server{
		Addr:    socket,
		Handler: mux,
	}

	log.Printf("Listening on %s", socket)

	if *httpsPtr {
		if *pubPtr == "" || *keyPtr == "" {
			fmt.Fprintf(os.Stderr, "Error: HTTPS flag provided without valid public and private options")
			os.Exit(1)
		}

		log.Fatal(server.ListenAndServeTLS(*pubPtr, *keyPtr))
	} else {
		log.Fatal(server.ListenAndServe())
	}
}


func handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")

	addr := r.RemoteAddr
	realIP := r.Header.Get("X-Real-IP")
	if realIP != "" {
		addr = realIP
	}

	data := AliasData{
		Alias: alias.RandomAlias(),
	}
	tmpl, err := template.ParseFS(content, "layout.html")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
	tmpl.Execute(w, data)

	log.Printf("%s - %s", addr, data.Alias)
}

func apiHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	addr := r.RemoteAddr
	realIP := r.Header.Get("X-Real-IP")
	if realIP != "" {
		addr = realIP
	}

	data := AliasData{
		Alias: alias.RandomAlias(),
	}
	json.NewEncoder(w).Encode(data)

	log.Printf("%s - %s", addr, data.Alias)
}
