module gitlab.com/tuesdays/server

go 1.17

replace gitlab.com/tuesdays/alias => ../alias

require gitlab.com/tuesdays/alias v0.0.0-00010101000000-000000000000
